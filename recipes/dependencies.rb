node[:rtorrent][:package_dependencies].each do |name|
  package name
end

bash "SVN checkout xmlrpc-c" do
  cwd "/usr/local/src"
  code <<-EOH
  svn co http://svn.code.sf.net/p/xmlrpc-c/code/advanced/ xmlrpc-c
  cd xmlrpc-c
  ./configure --disable-cplusplus
  make && make install
  EOH
  not_if "which xmlrpc-c-config"
end

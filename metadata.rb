name "rtorrent"

description      "Installs & configures rtorrent & rutorrent."

depends "apt" # https://github.com/gchef/apt-cookbook
depends "bootstrap" # https://github.com/gchef/bootstrap-cookbook
depends "build-essential"

recipe  "rtorrent", "General setup"
recipe  "rtorrent::rutorrent", "rutorrent web gui"
